
## Installation

**Clone with `nextflow`:**

```
git clone https://gitlab.com/tdbiggs/geneselectornf
```

**Test a single run:**

```
srun python bin/boruta_runner.py \
    --gem_netcdf "/scidas/bovine-cvm/bovine-project-cvm_cahnrs/04-Create_GEM/bovine_GEM.nc" \
    --x_label "counts" \
    --y_label "health_status" \
    --boruta_opts '{"perc": 90, "max_iter": 10}' \
    --model_opts '{"class_weight": "balanced", "max_depth": 3}'
```


**Run the workflow:**

```
# Run the workflow.
nextflow run https://gitlab.com/tdbiggs/geneselectornf \
    -profile kamiak \
    -c ./nextflow.config \
    -with-report \
```
