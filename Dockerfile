FROM python:3.7

RUN pip install -U click pandas numpy xarray scikit-learn Boruta netcdf4

# TAG
# tylerbiggs/GeneSelector:latest

# Build command
# docker build -t tylerbiggs/GeneSelector:latest .

# Push command
# docker push tylerbiggs/GeneSelector:latest
